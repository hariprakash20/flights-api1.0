const express = require("express");
const createError = require("http-errors");
const db = require("../models");
const Flight = db.flights;
const Op = db.Sequelize.Op;

exports.create = (req, res, next) => {
   
  if (
    !req.body.airlines ||
    !req.body.fromAirport ||
    !req.body.toAirport ||
    !req.body.depTime ||
    !req.body.arrTime ||
    !req.body.fare
  ) {
    next(
      createError(400, {
        message: "Invalid request! "+ JSON.stringify(req.body),
      })
    );
    return;
  }

  const flight = {
    airlines: req.body.airlines,
    fromAirport: req.body.fromAirport,
    toAirport: req.body.toAirport,
    depTime: req.body.depTime,
    arrTime: req.body.arrTime,
    fare: req.body.fare,
  };

  Flight.create(flight)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next(
        createError(500, {
          message:
            err.message || "some error occured while creating the Flight data",
        })
      );
    });
};

exports.findAll = (req, res, next) => {
  Flight.findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      next(
        createError(500, {
          message:
            err.message || "some error occured during fetching the data.",
        })
      );
    });
};

exports.findOne = (req, res, next) => {
    const id = req.params.id;
  
    Flight.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find Flight with id=${id}.`
          });
        }
      })
      .catch(err => {
        next(
            createError(500,{
                message : err.message || "Error occured while retrieving Flight data"
            })
        )
      });
  };

  exports.update = (req, res, next) => {
    const id = req.params.id;
  
    Flight.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Flight was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Flight with id=${id}. Maybe Flight was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        next(
            createError(500,{
                message: err.message || "Error updating Flight with id=" + id
            })
        )
      });
  };