const { DATABASE_URL} = require('../config/config')

const Sequelize = require("sequelize");
const sequelize = new Sequelize(DATABASE_URL);
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.flights = require("./flight.model")(sequelize, Sequelize);

module.exports = db;