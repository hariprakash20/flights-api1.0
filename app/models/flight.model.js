module.exports = (sequelize, Sequelize) => {
    const Flight = sequelize.define('flights', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            allowNull: false,
            primaryKey: true,
        },
        airlines: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        fromAirport:{
            type: Sequelize.STRING,
            allowNull: false,
        },
        toAirport:{
            type: Sequelize.STRING,
            allowNull: false,   
        },
        depTime:{
            type: Sequelize.STRING,
            allowNull:false,
        },
        arrTime:{
            type: Sequelize.STRING,
            allowNull:false,
        },
        fare:{
            type:Sequelize.INTEGER,
            allowNull: false,
        },
        imageUrl:{
            type:Sequelize.STRING,
        },
        seatsAvailabilityStatus:{
            type:Sequelize.STRING,
            get: function () {
                return JSON.parse(this.getDataValue('seatsAvailabilityStatus'));
            },
            set: function (value) {
                this.setDataValue('seatsAvailabilityStatus', JSON.stringify(value));
            }
        }
    }, {
        timestamps: false,
    })
    return Flight;
}