module.exports = (app) =>{
    const flights = require('../controllers/filght.controller');

    const router = require("express").Router();

    router.post("/", flights.create);

    router.get("/", flights.findAll);

    router.get("/:id", flights.findOne);

    router.put("/:id", flights.update);

    app.use('/api/flights', router);
}