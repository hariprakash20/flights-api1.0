const express = require('express');
const cors = require('cors');
const db = require('./app/models');
const apiErrorHandler = require('./app/error/api-error-handler')

const app = express();

let corsOptions = {
    origin : '*'
}

app.use(cors(corsOptions));

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

db.sequelize.sync()
  .then(() => {
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });

  app.get("/", (req, res) => {
    res.json({ message: "Welcome to flights API." });
  });

  require("./app/routes/flight.routes")(app);

  app.use(apiErrorHandler);

const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});